# Cadastro de Usuário

## Funcionalidade: Cadastro de Usuário

### Cenário: Cadastro bem-sucedido de um novo usuário
  - Dado que o usuário acessa a página de cadastro
  - Quando o usuário preenche os seguintes campos:
    - Nome: [Nome do Usuário]
    - Email: [Endereço de Email]
    - Senha: [Senha]
    - Data de Nascimento: [Data de Nascimento]
  - E clica no botão de cadastro
  - Então o usuário deve ser redirecionado para a página inicial
  - E o usuário deve receber uma mensagem de boas-vindas

### Cenário: Tentativa de cadastro com email já existente
  - Dado que o usuário acessa a página de cadastro
  - E um usuário com o email [Endereço de Email] já está cadastrado
  - Quando o usuário preenche os seguintes campos:
    - Nome: [Nome do Usuário]
    - Email: [Endereço de Email]
    - Senha: [Senha]
    - Data de Nascimento: [Data de Nascimento]
  - E clica no botão de cadastro
  - Então o usuário deve receber uma mensagem de erro informando que o email já está em uso
  - E o usuário permanece na página de cadastro

### Cenário: Cadastro sem preenchimento de todos os campos obrigatórios
  - Dado que o usuário acessa a página de cadastro
  - Quando o usuário não preenche todos os campos obrigatórios
  - E clica no botão de cadastro
  - Então o usuário deve receber uma mensagem de erro informando que todos os campos obrigatórios devem ser preenchidos
  - E o usuário permanece na página de cadastro
