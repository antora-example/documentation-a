# Cadastro de Produto

## Funcionalidade: Cadastro de Produto

### Cenário: Cadastro bem-sucedido de um novo produto
  - Dado que o usuário tem acesso à página de cadastro de produtos
  - Quando o usuário preenche os seguintes campos:
    - Nome do Produto: [Nome do Produto]
    - Valor: [Valor do Produto]
    - Quantidade em Estoque: [Quantidade em Estoque]
  - E clica no botão de cadastro
  - Então o usuário deve receber uma mensagem de confirmação de cadastro
  - E o produto deve ser listado na página de produtos cadastrados

### Cenário: Cadastro com valor inválido
  - Dado que o usuário tem acesso à página de cadastro de produtos
  - Quando o usuário preenche os seguintes campos:
    - Nome do Produto: [Nome do Produto]
    - Valor: [Valor Inválido]
    - Quantidade em Estoque: [Quantidade em Estoque]
  - E clica no botão de cadastro
  - Então o usuário deve receber uma mensagem de erro informando que o valor inserido é inválido
  - E o usuário permanece na página de cadastro

### Cenário: Cadastro sem preenchimento de todos os campos obrigatórios
  - Dado que o usuário tem acesso à página de cadastro de produtos
  - Quando o usuário não preenche todos os campos obrigatórios
  - E clica no botão de cadastro
  - Então o usuário deve receber uma mensagem de erro informando que todos os campos obrigatórios devem ser preenchidos
  - E o usuário permanece na página de cadastro
