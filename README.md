# Projeto inicial de Antora Documentation

Este projeto tem como objetivo entender o funcionamento do Antora Documentation para aplicar com a escrita de BDD.

Neste repositório é criado todas as documentações em BDD na qual é gerado o site a partir do repositório https://gitlab.com/antora-example/doc-site-antora 


- Para visualizar a pagina buildada basta digitar https://bdd-teste.surge.sh/ no navegador.



## Comandos de conversão de markdown para asciidoc

- Primeiro converte de markdown para HTLM: `pandoc -s *.md -o *.html `
- Depois converter de HTML para asciidoc: `pandoc --wrap=none -f html -t asciidoc *.html > ../documentation/modules/ROOT/pages/*.adoc` 